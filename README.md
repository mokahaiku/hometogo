# Hometogo task

The web app is a React Redux app. There is just a single page located at `/`. The app loads a Navbar and the initial 4 components documented in the task description. There is a selectbox and a button in the Navbar, which adds new components. The components behave as required in the task description - the ButtonCard component calls one of the APIs on click, the ListCard component displays the results of the API call, the StateCard component shows the state of the application and the CheckboxCard selects, which API to call.

## Requirements

- Linux or macOS or Windows
- Node 8.8.0 or later
- Yarn 1.2.0 or later
- Any browser, which supports [CSS grid](https://caniuse.com/#feat=css-grid)

## Setup

- Setup: `cd hometogo && yarn`
- Dev run: `yarn dev`
- Build: `yarn build`

## Technologies

You can view the dependencies used in this project in the `package.json` file.

- React
- Redux
- Jest
- Material UI

## Issues

If there are any issues with the project, please, inform me via email dovydas.ceilutka@gmail.com

## License

This project is private and unlicensed, do not distribute it without my permission.
