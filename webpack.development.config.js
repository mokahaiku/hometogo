const path = require('path');
const webpack = require('webpack');
const DashboardPlugin = require('webpack-dashboard/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PATH_SRC = path.resolve(__dirname, './src');
const PATH_BUILD = path.resolve(__dirname, './build');

function srcPath(subdir) {
  return path.join(PATH_SRC, subdir);
}

module.exports = {
  mode: 'development',
  cache: true,
  devtool: 'eval-source-map',
  devServer: {
    hot: true,
    overlay: true
  },
  entry: {
    app: ['./src/index.tsx'],
  },
  output: {
    path: PATH_BUILD,
    filename: '[name].js',
    publicPath: '/',
  },
  resolve: {
    alias: {
      Components: srcPath('components'),
      Constants: srcPath('constants'),
      Actions: srcPath('actions'),
      Reducers: srcPath('reducers'),
      Store: srcPath('store'),
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    modules: ['src', 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        include: PATH_SRC,
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              compilerOptions: {
                sourceMap: true,
                target: 'es5',
                isolatedModules: true,
                noEmitOnError: false,
              },
            },
          },
        ],
      },
      {
        test: /\.json$/,
        include: [PATH_SRC],
        use: { loader: 'json-loader' },
      },
    ],
  },
  plugins: [
    new DashboardPlugin(),
    new HtmlWebpackPlugin({ template: './index.html' }),
    new webpack.HotModuleReplacementPlugin(),
  ],
};
