module.exports = {
  setupFiles: ['<rootDir>/jest.setup.ts'],
  setupTestFrameworkScriptFile: './node_modules/jest-enzyme/lib/index.js',
  testPathIgnorePatterns: ['<rootDir>/node_modules/'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  moduleNameMapper: {
    'Components/(.*)': '<rootDir>/src/components/$1',
    'Actions/(.*)': '<rootDir>/src/actions/$1',
    'Constants/(.*)': '<rootDir>/src/constants/$1',
    'Reducers/(.*)': '<rootDir>/src/reducers/$1',
    'Store/(.*)': '<rootDir>/src/store/$1',
  },
  transform: {
    '.(ts|tsx)': './node_modules/ts-jest/preprocessor.js',
  },
  globals: {
    window: {},
    'ts-jest': {
      tsConfigFile: './tsconfig.json',
    },
  },
  testRegex: '(/spec/.*|\\.(test|spec))\\.(ts|tsx|js)$',
  moduleFileExtensions: ['js', 'jsx', 'json', 'ts', 'tsx'],
};
