const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const PATH_SRC = path.resolve(__dirname, './src');
const PATH_BUILD = path.resolve(__dirname, './build');

function srcPath(subdir) {
  return path.join(PATH_SRC, subdir);
}

module.exports = {
  mode: 'production',
  cache: true,
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    overlay: true,
  },
  entry: {
    app: ['./src/index.tsx'],
  },
  output: {
    path: PATH_BUILD,
    filename: '[name].bundle.js',
    publicPath: '/',
  },
  resolve: {
    alias: {
      Components: srcPath('components'),
      Constants: srcPath('constants'),
      Actions: srcPath('actions'),
      Reducers: srcPath('reducers'),
      Store: srcPath('store'),
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    modules: ['src', 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        include: PATH_SRC,
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              compilerOptions: {
                sourceMap: true,
                target: 'es5',
                isolatedModules: true,
                noEmitOnError: false,
              },
            },
          },
        ],
      },
      {
        test: /\.json$/,
        include: [PATH_SRC],
        use: { loader: 'json-loader' },
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(['build']),
    new HtmlWebpackPlugin({ template: './index.html' }),
    new UglifyJSPlugin({
      sourceMap: true,
    }),
  ],
};
