import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { ListCard } from '.';

const props = {
  classes: {
    container: 'someClassName',
  },
  list: [
    {
      text: 'someText',
      value: false,
    },
  ],
  onChange: () => null,
  value: false,
};

describe('ListCard', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<ListCard {...props} />);
    expect(wrapper).toExist();
    expect(wrapper).toMatchSnapshot();
  });
});
