import {
  Checkbox,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Paper,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { checkListItem } from 'Actions/list';
import React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

const styles = () => ({
  container: {
    gridRow: 'span 4',
  },
});

export interface IListCardProps {
  classes: {
    container: string;
  };
  list: Array<{ text: string; value?: boolean }>;
  value: boolean;
  onChange: (index: number, checked: boolean) => any;
}

export const ListCard: React.SFC<IListCardProps> = ({
  classes,
  list,
  onChange,
}) => {
  return (
    <Paper className={classes.container}>
      <List>
        <ListSubheader disableSticky={true}>
          Selected item count: {list.reduce((r, v) => r + Number(v.value), 0)}
        </ListSubheader>
        {list.map(({ value, text }, index) => (
          <ListItem key={index}>
            <Checkbox checked={value} onChange={onChange(index)} />
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </Paper>
  );
};

const mapStateToProps = ({
  list,
}: {
  list: { value?: boolean; text: string };
}) => ({ list });

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onChange: (itemIndex: number) => (e: any, value: boolean) =>
    dispatch(checkListItem(itemIndex, value)),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withStyles(styles),
)(ListCard);
