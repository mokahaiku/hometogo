import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { CheckboxCard } from '.';

const props = {
  classes: {
    container: 'someClassName',
  },
  onCheckboxToggle: () => null,
  value: false,
};

describe('CheckboxCard', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<CheckboxCard {...props} />);
    expect(wrapper).toExist();
    expect(wrapper).toMatchSnapshot();
  });
});
