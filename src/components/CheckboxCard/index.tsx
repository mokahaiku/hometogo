import { Checkbox, FormControlLabel, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { checkAlternativeAPI } from 'Actions/components';
import React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

const styles = () => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    padding: '1rem',
  },
});

export interface ICheckboxCardProps {
  classes: {
    container: string;
  };
  value: boolean;
  onCheckboxToggle: () => any;
}

export const CheckboxCard: React.SFC<ICheckboxCardProps> = ({
  classes,
  value,
  onCheckboxToggle,
}) => {
  return (
    <Paper className={classes.container}>
      <FormControlLabel
        control={
          <Checkbox
            checked={value}
            onChange={onCheckboxToggle}
            color="primary"
          />
        }
        label="Use alternative API"
      />
    </Paper>
  );
};

const mapDispatchToProps = (
  dispatch: Dispatch,
  { componentIndex }: { componentIndex: number },
) => {
  return {
    onCheckboxToggle: ({
      target: { checked },
    }: {
      target: { checked: boolean };
    }) => {
      dispatch(checkAlternativeAPI(componentIndex, checked));
    },
  };
};

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withStyles(styles),
)(CheckboxCard);
