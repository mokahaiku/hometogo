import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { Homepage } from '.';

describe('Homepage', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<Homepage components={[]} />);
    expect(wrapper).toExist();
    expect(wrapper).toMatchSnapshot();
  });
});
