import { addComponent } from 'Actions/components';
import React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import ButtonCard from '../ButtonCard';
import CheckboxCard from '../CheckboxCard';
import Layout from '../Layout';
import ListCard from '../ListCard';
import Navbar from '../Navbar';
import StateCard from '../StateCard';

const componentMap = {
  ButtonCard,
  CheckboxCard,
  ListCard,
  StateCard,
};

interface IComponent {
  name: string;
  value?: boolean;
}

export interface IHomepageProps {
  classes: {
    container: string;
  };
  components: IComponent[];
}

export const Homepage: React.SFC<IHomepageProps> = ({ components }) => (
  <Layout>
    <Navbar componentTypes={Object.keys(componentMap)} />
    {components.map(({ name }, i) => {
      const T = componentMap[name];
      return <T key={i} componentIndex={i} />;
    })}
  </Layout>
);

const mapStateToProps = ({ components }: { components: IComponent[] }) => ({
  components,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onClick: (componentName: string) => () => {
    dispatch(addComponent(componentName));
  },
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Homepage);
