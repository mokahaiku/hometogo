import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { ButtonCard } from '.';

const props = {
  classes: {
    container: 'someClassName',
  },
  components: [
    {
      name: 'someName',
    },
  ],
  onClick: () => null,
};

describe('ButtonCard', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<ButtonCard {...props} />);
    expect(wrapper).toExist();
    expect(wrapper).toMatchSnapshot();
  });
});
