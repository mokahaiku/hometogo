import { Button, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { callAPI } from 'Actions/list';
import React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

const styles = () => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    padding: '1rem',
  },
});

interface IComponents {
  name: string;
  value?: boolean;
}

export interface IButtonCardProps {
  classes: {
    container: string;
  };
  components: IComponents[];
  onClick: (isAlternativeAPI: boolean) => any;
}

export const ButtonCard: React.SFC<IButtonCardProps> = ({
  components,
  classes,
  onClick,
}) => {
  return (
    <Paper className={classes.container}>
      <Button
        variant="raised"
        color="primary"
        onClick={onClick(
          components.reduce(
            (result, { name, value }) =>
              name === 'CheckboxCard' ? result && value : result,
            true,
          ),
        )}
      >
        Call API
      </Button>
    </Paper>
  );
};

const mapStateToProps = ({ components }: { components: IComponents[] }) => ({
  components,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onClick: (isAlternativeAPI: boolean) => () =>
    dispatch(callAPI(isAlternativeAPI)),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withStyles(styles),
)(ButtonCard);
