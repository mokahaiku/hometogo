import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { Layout } from '.';

const props = {
  classes: {
    container: 'someClassName',
    innerContainer: 'someClassName',
  },
};

describe('Layout', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<Layout {...props} />);
    expect(wrapper).toExist();
    expect(wrapper).toMatchSnapshot();
  });
});
