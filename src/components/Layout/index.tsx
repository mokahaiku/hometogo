import { blueGrey } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';

const styles = () => ({
  container: {
    backgroundColor: blueGrey['50'],
    minHeight: '100vh',
    padding: '1rem',
  },
  innerContainer: {
    display: 'grid',
    gridGap: '1rem',
    gridTemplateColumns: '1fr 1fr',
    margin: 'auto',
    maxWidth: '900px',
  },
});

export interface ILayoutProps {
  classes: {
    container: string;
    innerContainer: string;
  };
}

export const Layout: React.SFC<ILayoutProps> = ({ children, classes }) => (
  <div className={classes.container}>
    <div className={classes.innerContainer}>{children}</div>
  </div>
);

export default withStyles(styles)(Layout);
