import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { Navbar } from '.';

const props = {
  classes: {
    container: 'someClassName',
  },
  componentTypes: ['ComponentA'],
  onAddComponentClick: () => null,
  onSelectedComponentChange: () => null,
  selectedComponent: 'ComponentA',
};

describe('Navbar', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<Navbar {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});
