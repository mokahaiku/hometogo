import { Button, MenuItem, Paper, Select } from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { addComponent, updateSelectedComponent } from 'Actions/components';
import React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

const styles = () => ({
  container: {
    display: 'grid',
    gridColumn: '1/-1',
    gridGap: '1rem',
    gridTemplateColumns: '3fr 1fr',
    padding: '1rem 2rem',
  },
});

export interface INavbarProps {
  classes: {
    container: string;
  };
  componentTypes: string[];
  onAddComponentClick: (selectedComponent: string) => any;
  onSelectedComponentChange: () => any;
  selectedComponent: string;
}

export const Navbar: React.SFC<INavbarProps> = ({
  classes,
  componentTypes,
  onAddComponentClick,
  onSelectedComponentChange,
  selectedComponent,
}) => {
  return (
    <Paper className={classes.container}>
      <Select value={selectedComponent} onChange={onSelectedComponentChange}>
        {componentTypes.map(componentType => (
          <MenuItem key={componentType} value={componentType}>
            {componentType}
          </MenuItem>
        ))}
      </Select>
      <Button
        variant="raised"
        onClick={onAddComponentClick(selectedComponent)}
        color="primary"
      >
        Add Component
      </Button>
    </Paper>
  );
};

const mapStateToProps = ({
  selectedComponent,
}: {
  selectedComponent: string;
}) => ({ selectedComponent });

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onAddComponentClick: (componentType: string) => () =>
      dispatch(addComponent(componentType)),
    onSelectedComponentChange: ({
      target: { value },
    }: {
      target: { value: string };
    }) => dispatch(updateSelectedComponent(value)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withStyles(styles),
)(Navbar);
