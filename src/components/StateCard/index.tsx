import { Paper, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { green, grey, red, yellow } from '@material-ui/core/colors';
import classNames from 'classnames';
import * as states from 'Constants/states';
import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

interface IStateMap {
  [key: string]: string;
}

const stateMap: IStateMap = {
  [states.IDLE]: 'idleContainer',
  [states.LOADING]: 'loadingContainer',
  [states.LOADED]: 'loadedContainer',
  [states.ERROR]: 'errorContainer',
};

const styles = () => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    padding: '1rem',
  },
  errorContainer: {
    backgroundColor: red['600'],
  },
  idleContainer: {
    backgroundColor: grey['500'],
  },
  loadedContainer: {
    backgroundColor: green['600'],
  },
  loadingContainer: {
    backgroundColor: yellow['800'],
  },
  text: {
    color: 'white',
  },
});

export interface IStatusCardProps {
  classes: {
    container: string;
    errorContainer: string;
    idleContainer: string;
    loadedContainer: string;
    loadingContainer: string;
    text: string;
  };
  state: string;
}

export const StateCard: React.SFC<IStatusCardProps> = ({ classes, state }) => {
  return (
    <Paper className={classNames(classes.container, classes[stateMap[state]])}>
      <Typography className={classes.text} variant="subheading">{state}</Typography>
    </Paper>
  );
};

const mapStateToProps = ({ state }: { state: string }) => ({
  state,
});

export default compose(
  connect(mapStateToProps),
  withStyles(styles)
)(StateCard);
