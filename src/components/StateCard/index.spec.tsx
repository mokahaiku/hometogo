import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { StateCard } from '.';

const props = {
  classes: {
    container: 'someClassName0',
    errorContainer: 'someClassName4',
    idleContainer: 'someClassName1',
    loadedContainer: 'someClassName3',
    loadingContainer: 'someClassName2',
    text: 'someClassName5',
  },
  state: 'IDLE',
};

describe('StateCard', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<StateCard {...props} />);
    expect(wrapper).toExist();
    expect(wrapper).toMatchSnapshot();
  });
});
