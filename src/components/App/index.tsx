/* tslint:disable:jsx-no-lambda */

import { CssBaseline } from '@material-ui/core';
import { blue, red } from '@material-ui/core/colors';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Homepage } from 'Components/index';
import * as React from 'react';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { browserHistory, store } from 'Store/index';

const theme = createMuiTheme({
  palette: {
    primary: { main: blue['700'] },
    secondary: { main: red['700'] },
  },
});

export class App extends React.Component<{}, {}> {
  public render() {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Provider store={store}>
          <ConnectedRouter history={browserHistory}>
            <Route exact={true} path="/" render={() => <Homepage />} />
          </ConnectedRouter>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
