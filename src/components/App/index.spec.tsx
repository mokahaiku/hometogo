import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';
import { App } from '.';

describe('App', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toExist();
    expect(wrapper).toMatchSnapshot();
  });
});
