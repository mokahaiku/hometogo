import { IDLE } from 'Constants/states';

export interface IState {
  readonly components: Array<{ name: string; value?: boolean }>;
  readonly list: string[];
  readonly selectedComponent: string;
  readonly state: string;
}

export const initialState: IState = {
  components: [
    { name: 'ButtonCard' },
    { name: 'CheckboxCard', value: false },
    { name: 'ListCard' },
    { name: 'StateCard' },
  ],
  list: [],
  selectedComponent: 'ButtonCard',
  state: IDLE,
};
