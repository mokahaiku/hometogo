import { CHECK_LIST_ITEM, UPDATE_LIST } from 'Constants/actionTypes';
import { initialState } from './initialState';
import listReducer from './listReducer';

describe('listReducer', () => {
  test('returns initial state', () => {
    const action = {
      payload: [],
      type: 'SOME_OTHER_ACTION_TYPE',
    };
    expect(listReducer(undefined, action)).toEqual(initialState.list);
  });

  test('returns updated state on update list action', () => {
    const payload = [{ text: 'Some text', value: false }];
    const action = {
      payload,
      type: UPDATE_LIST,
    };
    expect(listReducer(undefined, action)).toEqual(payload);
  });

  test('returns updated state on check list item action', () => {
    const payload = {
      itemIndex: 1,
      value: true,
    };
    const action = {
      payload,
      type: CHECK_LIST_ITEM,
    };
    const startingState = [
      {
        text: 'Some text',
        value: false,
      },
      {
        text: 'Some text',
        value: false,
      },
    ];
    const expectedState = [
      {
        text: 'Some text',
        value: false,
      },
      {
        text: 'Some text',
        value: true,
      },
    ];
    expect(listReducer(startingState, action)).toEqual(expectedState);
  });
});
