import { ADD_COMPONENT, CHECK_ALTERNATIVE_API } from 'Constants/actionTypes';
import componentsReducer from './componentsReducer';
import { initialState } from './initialState';

describe('componentsReducer', () => {
  test('returns initial state', () => {
    const action = {
      payload: [{ name: 'Component A' }],
      type: 'SOME_OTHER_ACTION_TYPE',
    };
    expect(componentsReducer(undefined, action)).toEqual(
      initialState.components,
    );
  });

  test('returns updated state on add component action', () => {
    const action = {
      payload: [{ name: 'Component A' }],
      type: ADD_COMPONENT,
    };
    expect(componentsReducer(undefined, action)).toEqual([
      ...initialState.components,
      action.payload,
    ]);
  });

  test('returns updated state on check alternative API action', () => {
    const payload = {
      componentIndex: 1,
      value: true,
    };
    const action = {
      payload,
      type: CHECK_ALTERNATIVE_API,
    };
    const startingState = [
      {
        name: 'ComponentA',
        value: false,
      },
      {
        text: 'ComponentB',
        value: false,
      },
    ];
    const expectedState = [
      {
        name: 'ComponentA',
        value: false,
      },
      {
        text: 'ComponentB',
        value: true,
      },
    ];
    expect(componentsReducer(startingState, action)).toEqual(expectedState);
  });
});
