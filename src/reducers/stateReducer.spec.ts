import { UPDATE_STATE } from 'Constants/actionTypes';
import { initialState } from './initialState';
import stateReducer from './stateReducer';

describe('stateReducer', () => {
  test('returns initial state', () => {
    const action = {
      payload: 'payload',
      type: 'SOME_OTHER_ACTION_TYPE',
    }
    expect(stateReducer(undefined, action)).toEqual(initialState.state);
  });

  test('returns updated state on update state action', () => {
    const payload = 'newState';
    const action = {
      payload,
      type: UPDATE_STATE,
    }
    expect(stateReducer(undefined, action)).toEqual(payload);
  });
})
