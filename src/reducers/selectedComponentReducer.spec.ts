import { UPDATE_SELECTED_COMPONENT } from 'Constants/actionTypes';
import { initialState } from './initialState';
import selectedComponentReducer from './selectedComponentReducer';

describe('selectedComponentReducer', () => {
  test('returns initial state', () => {
    const action = {
      payload: 'payload',
      type: 'SOME_OTHER_ACTION_TYPE',
    };
    expect(selectedComponentReducer(undefined, action)).toEqual(
      initialState.selectedComponent,
    );
  });

  test('returns updated state on update selected component action', () => {
    const payload = 'ComponentA';
    const action = {
      payload,
      type: UPDATE_SELECTED_COMPONENT,
    };
    expect(selectedComponentReducer(undefined, action)).toEqual(payload);
  });
});
