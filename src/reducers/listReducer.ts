import { CHECK_LIST_ITEM, UPDATE_LIST } from 'Constants/actionTypes';
import { initialState } from './initialState';

interface IAction {
  type: string;
  payload: any;
}

export default function listReducer(
  state = initialState.list,
  action: IAction,
) {
  switch (action.type) {
    case UPDATE_LIST:
      return action.payload.map(({ text }: { text: string }) => ({
        text,
        value: false,
      }));

    case CHECK_LIST_ITEM:
      return state.map(
        (item, index) =>
          action.payload.itemIndex === index
            ? { ...item, value: action.payload.value }
            : item,
      );

    default:
      return state;
  }
}
