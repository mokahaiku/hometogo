import { UPDATE_STATE } from 'Constants/actionTypes';
import { initialState } from './initialState';

interface IAction {
  type: string;
  payload: string;
}

export default function stateReducer(
  state: string = initialState.state,
  action: IAction
): string {
  switch (action.type) {
    case UPDATE_STATE:
      return action.payload;

    default:
      return state;
  }
}
