import { UPDATE_SELECTED_COMPONENT } from 'Constants/actionTypes';
import { initialState } from './initialState';

interface IAction {
  type: string;
  payload: string;
}

export default function selectedComponentReducer(
  state: string = initialState.selectedComponent,
  action: IAction,
): string {
  switch (action.type) {
    case UPDATE_SELECTED_COMPONENT:
      return action.payload;

    default:
      return state;
  }
}
