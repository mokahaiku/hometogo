import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import components from './componentsReducer';
import list from './listReducer';
import selectedComponent from './selectedComponentReducer';
import state from './stateReducer';

const rootReducer = combineReducers({
  components,
  list,
  routing: routerReducer,
  selectedComponent,
  state,
});

export default rootReducer;
