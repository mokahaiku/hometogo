import { ADD_COMPONENT, CHECK_ALTERNATIVE_API } from 'Constants/actionTypes';
import { initialState } from './initialState';

interface IAction {
  type: string;
  payload: any;
}

export default function componentsReducer(
  state = initialState.components,
  action: IAction,
) {
  switch (action.type) {
    case ADD_COMPONENT:
      return [...state, action.payload];

    case CHECK_ALTERNATIVE_API:
      return state.map(
        (component, index) =>
          action.payload.componentIndex === index
            ? { ...component, value: action.payload.value }
            : component,
      );

    default:
      return state;
  }
}
