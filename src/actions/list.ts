import { updateState } from 'Actions/state';
import axios from 'axios';
import { CHECK_LIST_ITEM, UPDATE_LIST } from 'Constants/actionTypes';
import { ERROR, LOADED, LOADING } from 'Constants/states';
import { Dispatch } from 'redux';

const API_1_URL = 'https://www.anapioficeandfire.com/api/houses';
const API_2_URL = 'https://swapi.co/api/people';

interface IListItem {
  text: string;
}

export const loadData = async (url: string) => {
  const { status, data } = await axios(url);
  return {
    list: (data.results || data).map(({ name }: { name: string }) => ({
      text: name,
    })),
    status,
  };
};

export const updateList = (list: IListItem[]) => ({
  payload: list,
  type: UPDATE_LIST,
});

export const checkListItem = (itemIndex: number, value: boolean) => ({
  payload: {
    itemIndex,
    value,
  },
  type: CHECK_LIST_ITEM,
});

export const callAPI = (isAlternativeAPI: boolean) => async (
  dispatch: Dispatch,
) => {
  try {
    dispatch(updateState(LOADING));
    const { status, list } = await loadData(
      isAlternativeAPI ? API_2_URL : API_1_URL,
    );
    if (status !== 200) {
      return dispatch(updateState(ERROR));
    }
    dispatch(updateState(LOADED));
    return dispatch(updateList(list));
  } catch (error) {
    return dispatch(updateState(ERROR));
  }
};
