import { UPDATE_STATE } from 'Constants/actionTypes';
import { updateState } from './state';

describe('updateState actions', () => {
  test('creates an action to update state', () => {
    const newState = 'SomeState';
    const expected = {
      payload: newState,
      type: UPDATE_STATE,
    }
    expect(updateState(newState)).toEqual(expected);
  })
});
