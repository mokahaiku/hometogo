import {
  ADD_COMPONENT,
  CHECK_ALTERNATIVE_API,
  UPDATE_SELECTED_COMPONENT,
} from 'Constants/actionTypes';
import {
  addComponent,
  checkAlternativeAPI,
  updateSelectedComponent,
} from './components';

describe('addComponent actions', () => {
  test('creates an action to add a new CheckboxCard component', () => {
    const componentType = 'CheckboxCard';
    const expected = {
      payload: { name: componentType, value: false },
      type: ADD_COMPONENT,
    };
    expect(addComponent(componentType)).toEqual(expected);
  });

  test('creates an action to add a new non CheckboxCard component', () => {
    const componentType = 'ComponentA';
    const expected = {
      payload: { name: componentType },
      type: ADD_COMPONENT,
    };
    expect(addComponent(componentType)).toEqual(expected);
  });
});

describe('checkAlternativeAPI actions', () => {
  test('creates an action to add a new component', () => {
    const expected = {
      payload: {
        componentIndex: 1,
        value: true,
      },
      type: CHECK_ALTERNATIVE_API,
    };
    expect(checkAlternativeAPI(1, true)).toEqual(expected);
  });
});

describe('updateSelectedComponent actions', () => {
  test('creates an action to update the selected component', () => {
    const newComponent = 'ComponentA';
    const expected = {
      payload: newComponent,
      type: UPDATE_SELECTED_COMPONENT,
    };
    expect(updateSelectedComponent(newComponent)).toEqual(expected);
  });
});
