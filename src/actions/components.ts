import {
  ADD_COMPONENT,
  CHECK_ALTERNATIVE_API,
  UPDATE_SELECTED_COMPONENT,
} from 'Constants/actionTypes';

export const addComponent = (componentType: string) => ({
  payload:
    componentType === 'CheckboxCard'
      ? { name: componentType, value: false }
      : { name: componentType },
  type: ADD_COMPONENT,
});

export const checkAlternativeAPI = (
  componentIndex: number,
  value: boolean,
) => ({
  payload: {
    componentIndex,
    value,
  },
  type: CHECK_ALTERNATIVE_API,
});

export const updateSelectedComponent = (value: string) => ({
  payload: value,
  type: UPDATE_SELECTED_COMPONENT,
});
