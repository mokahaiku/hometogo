import { updateState } from 'Actions/state';
import axios from 'axios';
import { UPDATE_LIST } from 'Constants/actionTypes';
import { IDLE, LOADED, LOADING } from 'Constants/states';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { CHECK_LIST_ITEM } from './../constants/actionTypes';
import { callAPI, checkListItem, loadData, updateList } from './list';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
jest.mock('axios');

describe('loadData actions', () => {
  test('calls axios and returns formatted data', async () => {
    const response = {
      data: {
        results: [{ name: 'Rick and Morty' }],
      },
      status: 200,
    };
    axios.mockResolvedValue(response);
    const expected = {
      list: [{ text: 'Rick and Morty' }],
      status: response.status,
    };
    expect(await loadData('someurl')).toEqual(expected);
  });
});

describe('updateList actions', () => {
  test('creates an action to update list', () => {
    const newList = [{ text: 'Rick and Morty' }];
    const expected = {
      payload: newList,
      type: UPDATE_LIST,
    };
    expect(updateList(newList)).toEqual(expected);
  });
});

describe('checkListItem actions', () => {
  test('creates an action to check list item', () => {
    const expected = {
      payload: {
        itemIndex: 1,
        value: true,
      },
      type: CHECK_LIST_ITEM,
    };
    expect(checkListItem(1, true)).toEqual(expected);
  });
});

describe('callAPI actions', () => {
  test('dispatches correct actions', async () => {
    const response = {
      data: {
        results: [{ name: 'Rick and Morty' }],
      },
      status: 200,
    };
    axios.mockResolvedValue(response);
    const list = [{ text: 'Rick and Morty' }];
    const expectedActions = [
      updateState(LOADING),
      updateState(LOADED),
      updateList(list),
    ];
    const store = mockStore({ list: [], state: IDLE });
    await store.dispatch(callAPI(false));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
