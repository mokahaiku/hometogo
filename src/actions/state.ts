import { UPDATE_STATE } from 'Constants/actionTypes';

export const updateState = (newState: string) => ({
  payload: newState,
  type: UPDATE_STATE,
});
