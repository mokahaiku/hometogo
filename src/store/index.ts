import { createBrowserHistory } from 'history';
import { routerMiddleware as createRouterMiddleware } from 'react-router-redux';
import rootReducer from 'Reducers/index';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddlware from 'redux-thunk';

export const browserHistory = createBrowserHistory();
export const routerMiddleware = createRouterMiddleware(browserHistory);

const middlewares = [
  thunkMiddlware,
  routerMiddleware,
];

export const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(...middlewares),
));

export default store;

